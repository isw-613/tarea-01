<?php


class User
{
    private $connection;
    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function create($name, $lastname, $phone, $email)
    {
        $this->connection->runStatement('INSERT INTO users (name, lastname, phone, email) VALUES ($1, $2, $3, $4)', [$name, $lastname, $phone, $email]);
    }
}

